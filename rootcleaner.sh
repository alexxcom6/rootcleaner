#author: Alexander Sandoval
#date: 17 april 2022
#Description: Limpia temporales de sistemas linux y  cPanel
#Update: 17 april 2022

#!/bin/bash

echo Rootcleaner V2, limpia .trash de cPanel
wait 2

echo "limpiando carpeta cpanel"
> /usr/local/cpanel/logs/access_log
> /usr/local/cpanel/logs/login_log
rm -rf /usr/local/cpanel/logs/archive/*.gz
rm -rf /usr/local/cpanel/logs/update_analysis/*.gz
rm -rf /usr/local/cpanel/logs/cpbackup/*.log


echo "limpiando carpeta apache"
> /usr/local/apache/logs/access_log
> /usr/local/apache/logs/error_log
> /usr/local/apache/logs/suphp_log
rm -rf /usr/local/apache/logs/archive/*.gz

echo "limpiando carpeta var/log"
> /var/log/exim_mainlog
> /var/log/chkservd.log
> /var/log/exim_paniclog
> /var/log/exim_rejectlog
> /var/log/maillog
rm -rf /var/cache/logwatch/*

rm -rf /var/log/lve-stats.log-*
rm -rf/var/log/messages-*
rm -rf /var/log/maillog-*
rm -rf /var/log/*.gz
rm -rf /var/log/*.1
rm -rf /var/log/*.2
rm -rf /var/log/*.3
rm -rf /var/log/*.4

echo "carpeta root"
rm -rf /root/*Gs
rm -rf /root/*tgz
rm -rf /root/*rpm
rm -rf /root/*bz2
rm -rf /root/cuarentena/*

echo "limpiando comet_files"
/usr/local/cpanel/bin/purge_dead_comet_files

echo "limpiando virtfs"
/scripts/./clear_orphaned_virtfs_mounts --clearall

echo "limpiando temporal"
find /tmp -ctime +5 -type f -print -exec rm {} \;

echo "limpiando logaholic"
find /var/cpanel/userhomes/cpanellogaholic/ -type f -mtime +200 -exec rm -f {} \;

echo "limpiado cache de repositorios"
yum clean all

echo "limpiando cache de sitebuilder"
rm -rf /usr/local/cpanel/whostmgr/docroot/cgi/rvsitebuilderinstaller/packages/*tar.bz2

echo "limpiando imunify"
rm -rf /var/log/imunify360/captcha.log-*gz
rm -rf /var/log/imunify360/console.log.*
rm -rf /var/log/imunify360/captcha.log-*
rm -rf /var/log/imunify360/captcha/access.log-*


echo “Limpiando todos los cPanel”
#!/bin/bash
for account in {a..z}
  do
   rm -rfv /home/${account}*/mail/*/*/.Trash/cur/*
   rm -rfv /home/${account}*/.trash/*
   cd /home/${account}*/
   for item in $(find . -type f -name 'error_log' ) ;do cat /dev/null > $item ;done
done
for x in {1..9}
do
  for account in {a..z}
  do
   rm -rfv /home${x}/${account}*/mail/*/*/.Trash/cur/*
   rm -rfv /home${x}/${account}*/.trash/*
   cd /home${x}/${account}*/
   for item in $(find . -type f -name 'error_log' ) ;do cat /dev/null > $item ;done
  done
done

